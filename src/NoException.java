/*

Rombie Lagunzad
Fall 2020
Brother Lawrence

This simple program does not handle any exception if the user enter a 0 divisor or invalid values for the input.

a Create a program that gathers input of two numbers from the keyboard in its main method and then calls the method below to calculate.
b Write a method that divides the two numbers together (num1/num2) and returns the result. The method should include a throws clause for the most specific exception possible if the user enters zero for num2.
c In your main method, use exception handling to catch the most specific exception possible. Display a descriptive message in the catch to tell the user what the problem is. Give the user a chance to retry the data entry. Display a message in the final statement.
d Run and show the output when there is no exception and when a zero in the denominator is used.
e Rewrite the data collection so that it uses data validation to ensure an error does not occur. If the user does type a zero in the denominator, display a message and give the user a chance to retry.
f Run and show the output when there is no problem with data entry and when a zero is used in the denominator.

 */

import java.util.Scanner;

public class NoException {
    public static void main(String[] args) {

        System.out.println("\n*** Simple Division Calculator ***");

        Scanner input = new Scanner(System.in);

        System.out.println("\nPlease enter the first number:");
        int num1 = input.nextInt();
        System.out.println("Please enter the second number:");
        int num2 = input.nextInt();
        float quotient = doDivision(num1, num2);
        System.out.println("\nCalculation: "+num1+" / "+num2+ " = "+ quotient);

    }

    public static float doDivision(Integer num1, Integer num2)  {
        if (num2 == 0)
            throw new ArithmeticException("Divisor should not be zero");
        return num1 / num2;
    }
}


